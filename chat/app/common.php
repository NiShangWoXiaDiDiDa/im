<?php
// 应用公共文件

/**
 * 通用Api格式
 * @param $code
 * @param string $msg
 * @param array $data
 * @param int $httpCode
 * @return \think\response\Json
 */
function show($code, $msg = "", $data = [], $httpCode = 200){
    $result = [
        'code' => $code,
        'msg' => $msg,
        'data' => $data
    ];

    return json($result,$httpCode);
}
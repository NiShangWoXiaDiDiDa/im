<?php
/*
 * @Description: your project
 * @Author: xuyajun
 * @version: 1.0
 * @Date: 2023-06-05 11:15:11
 * @LastEditors: xuyajun
 * @LastEditTime: 2023-06-06 15:07:26
 */
// 全局中间件定义文件
return [
    // 全局请求缓存
    // \think\middleware\CheckRequestCache::class,
    // 多语言加载
    // \think\middleware\LoadLangPack::class,
    // Session初始化
    \think\middleware\SessionInit::class,

    // 跨域请求
    \think\middleware\AllowCrossDomain::class
];

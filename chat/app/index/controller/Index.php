<?php


namespace app\index\controller;


use app\BaseController;
use think\View;

class Index extends BaseController
{
    public function index(){
        $fromid =  input('fromid');
        $toid = input('toid');
        return view('index/index',['fromid'=>$fromid,'toid'=>$toid]);
    }

    public function lists(){
        $fromid =  input('fromid');
        return view('index/lists',['fromid'=>$fromid]);
    }
}
<?php
/*
 * @Description: your project
 * @Author: xuyajun
 * @version: 1.0
 * @Date: 2023-06-05 11:15:11
 * @LastEditors: xuyajun
 * @LastEditTime: 2023-06-06 14:50:28
 */

namespace app\api\service;
use Firebase\JWT\JWT;
use think\facade\Cache;
use think\facade\Session;
use think\Exception;
use think\exception\HttpException;

class Token
{
    //生成token
    public static function creatToken($uid){
        $key = config('token.tokenStr');         //这里是自定义的一个随机字串，应该写在config文件中的，解密时也会用，相当    于加密中常用的 盐  salt
        $token=array(
            "iss"=>$key,        //签发者 可以为空
            "aud"=>'',          //面象的用户，可以为空
            "iat"=>time(),      //签发时间
            "nbf"=>time(),    //在什么时间jwt开始生效
            "exp"=> time()+7200, //token 过期时间 两小时
            "data"=>[           //记录的userid的信息，这里是自已添加上去的，如果有其它信息，可以再添加数组的键值对
                'uid'=>$uid,
            ]
        );
        $jwt = JWT::encode($token, $key, "HS256");  //根据参数生成了 token
        return $jwt;
    }

    //验证token
    public static function verifyToken($token){
        $key = config('token.tokenStr');
        try {
            JWT::$leeway = 60; // 当前时间减去60，把时间留点余地
            $decoded = JWT::decode($token, $key, array('HS256')); //HS256方式，这里要和签发的时候对应
            $arr = (array)$decoded;
            $data = (array)$arr['data'];
            $uid = Session::get("uid");
            if($data['uid'] != $uid){
                throw new HttpException(500,"token令牌无效");
            }
        } catch(\Firebase\JWT\SignatureInvalidException $e) { //签名不正确
            throw new HttpException(200,"签名不正确");
        }catch(\Firebase\JWT\BeforeValidException $e) { // 签名在某个时间点之后才能用
            throw new HttpException(200,"登录过期，token失效");
        }catch(\Firebase\JWT\ExpiredException $e) { // token过期
            throw new HttpException(200,"登陆过期，token失效");
        }catch(Exception $e) { //其他错误
            throw new HttpException(500,"未知错误");
        }
    }
}
<?php


namespace app\api\model;


use think\exception\HttpException;
use app\api\enum\Communication as CommunicationEnum;

class Communication extends Base
{

    // 插入消息
    public static function add($datas){
        $res = self::insert($datas);
        if(!$res){
            throw new HttpException(500,'消息数据新增失败');
        }
        return $res;
    }

    // 获取聊天消息
    public static function messageLoad($uids){
        $msgs = self::where(function ($query) use ($uids) {
                $query->where([
                    'fromid'=>$uids['fromid'],
                    'toid'=>$uids['toid']
                ]);
            })
            ->whereOr(function ($query) use ($uids) {
                $query->where([
                    'fromid'=>$uids['toid'],
                    'toid'=>$uids['fromid']
                ]);
            })->order('time desc')->limit(10)->select();

        if(!$msgs){
            throw new HttpException(500,'用户不存在');
        }
        return $msgs;
    }

    // 查询当前聊天对象列表
    public static function getIdList($uid){
        $msglists1 = self::where('fromid',$uid)->field(['fromid','toid','fromname','toname'])->group('toid')->select()->toArray();
        $msglists2 = self::where('toid',$uid)->field(['fromid','toid','fromname','toname'])->group('fromid')->select()->toArray();

        $toids = array_map(function ($res){
            return $res['toid'];
        }, $msglists1);

        foreach ($msglists2 as $key => $value){
            if( in_array($value['fromid'],$toids )){
                unset($msglists2[$key]);
            }
        }


        $msglists = array_merge($msglists1,$msglists2);
        $user = new User();
        $rows = array_map(function ($res) use ($user,$uid){
            return [
                'headUrl'=> $uid != $res['fromid'] ? $user::getHeadOne($res['fromid']) : $user::getHeadOne($res['toid']),
                'username' => $uid != $res['fromid'] ? $res['fromname'] : $res['toname'],
                'countNoRead' => $uid != $res['fromid'] ? self::getCountNoRead($res['fromid'],$res['toid']) : self::getCountNoRead($res['toid'],$res['fromid']),
                'lastMessage' => $uid != $res['fromid'] ? self::getListMessage($res['fromid'],$res['toid']) : self::getListMessage($res['toid'],$res['fromid']),
                'toid' => $uid != $res['fromid'] ? $res['fromid'] : $res['toid']
            ];
        }, $msglists);

        return $rows;
    }

    // 获取联系人未读消息数
    public static function getCountNoRead($fromid,$toid){
        $res = self::where(['fromid'=>$fromid,'toid'=>$toid,'isread'=>0])->count();
        return $res;
    }

    // 获取联系人最后一条聊天数据
    public static function getListMessage($fromid,$toid){
        $res = self::where(function ($query) use ($fromid,$toid) {
                $query->where([
                    'fromid'=>$fromid,
                    'toid'=>$toid
                ]);
            })
            ->whereOr(function ($query) use ($fromid,$toid) {
                $query->where([
                    'fromid'=>$toid,
                    'toid'=>$fromid
                ]);
            })->order('id DESC')->limit(1)->select()->toArray();
        return $res;
    }

    // 消息标记已读状态
    public static function isRead($data){
        $where = [
            'fromid'=>$data['toid'],
            'toid'=>$data['fromid']
        ];
        $res = self::where($where)->update(['isread' => CommunicationEnum::read]);
        return $res;
    }

}
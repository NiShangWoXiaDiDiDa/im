<?php


namespace app\api\controller\v1;


use app\api\service\Token as TokenService;
use app\BaseController;
use think\exception\HttpException;
use app\api\model\User as UserModel;

class User extends BaseController
{
    /**
     * 用户登录
     */
    public function userLogin(){
        if(request()->isPost()){
            $res = UserModel::userLogin(input('post.'));
            if($res){
                return show(config('status.success'),"登录成功",$res,200);
            }
        }
        throw  new HttpException(404,'非法请求');
    }

    /**
     * 注册用户
     */
    public function userRegister(){
        if(request()->isPost()){
            $res = UserModel::userRegister(input('post.'));
            if($res){
                return show(config('status.success'),"注册成功",[],200);
            }
        }
        throw  new HttpException(404,'非法请求');
    }

    /**
     * 根据用户名查找用户
     */
    public function getUsernameList(){
        if (request()->isPost()){
            $token = request()->header('token');
            TokenService::verifyToken($token);

            $res = UserModel::getUsernameList(input('post.'));
            return show(config('status.success'),"ok",$res,200);
        }
        throw  new HttpException(404,'非法请求');
    }
}
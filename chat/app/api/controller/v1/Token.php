<?php
/*
 * @Description: your project
 * @Author: xuyajun
 * @version: 1.0
 * @Date: 2023-06-05 11:15:11
 * @LastEditors: xuyajun
 * @LastEditTime: 2023-06-05 16:02:35
 */


namespace app\api\controller\v1;


use app\BaseController;
use think\exception\HttpException;
use app\api\service\Token as TokenService;

class Token extends BaseController
{
    // 获取token
    public function getToken(){
        dd(123);
    }

    // 验证token
    public function verifyToken(){
        if(request()->isPost()){
            $token = request()->header('Token');
            if(!$token){
                throw new HttpException(500,'非法参数,token不存在');
            }
            TokenService::verifyToken($token);

            return show(config('status.success'), "ok", [], 200);
        }
    }
}
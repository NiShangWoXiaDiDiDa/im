<?php
/*
 * @Description: your project
 * @Author: xuyajun
 * @version: 1.0
 * @Date: 2023-06-05 11:15:11
 * @LastEditors: xuyajun
 * @LastEditTime: 2023-06-05 16:50:27
 */

use think\facade\Route;

Route::get('/getToken', 'api/v1.Token/getToken');

// 消息组
Route::group(':version/Chat',function(){
    Route::post('/getHeadImg', 'api/:version.Chat/getHeadImg');
    Route::post('/getNickName', 'api/:version.Chat/getNickName');
    Route::post('/messageLoad', 'api/:version.Chat/messageLoad');
    Route::post('/uploadImg', 'api/:version.Chat/uploadImg');
    Route::post('/getIdList', 'api/:version.Chat/getIdList');
    Route::post('/isRead', 'api/:version.Chat/isRead');
});

// 用户组
Route::group(':version/User',function(){
    Route::post('/userLogin', 'api/:version.User/userLogin');
    Route::post('/userRegister', 'api/:version.User/userRegister');
    Route::post('/getUsernameList', 'api/:version.User/getUsernameList');
});

// token获取，校验
Route::group(':version/Token',function(){
    Route::get('/getToken', 'api/:version.Token/getToken');
    Route::post('/verifyToken', 'api/:version.Token/verifyToken');
});

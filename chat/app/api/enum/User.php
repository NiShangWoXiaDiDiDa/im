<?php

namespace app\api\enum;

class User
{
    const type = 1; // 有效
    const ntype = 0; // 无效
    const M = 1; // 男
    const F = 2; // 女
}
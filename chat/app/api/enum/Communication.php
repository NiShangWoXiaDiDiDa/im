<?php


namespace app\api\enum;


class Communication
{
    const text = 1; // 文本
    const image = 2; // 图片
    const audio =3; // 语音

    const read = 1; // 已读
    const uread = 0; // 未读
}
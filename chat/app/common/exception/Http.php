<?php

namespace app\common\exception;
use think\exception\Handle;
use think\Response;
use Throwable;

class Http extends Handle
{
    public $httpStatus = 500;

    /**
     * Render an exception into an HTTP response.
     *
     * @access public
     * @param \think\Request   $request
     * @param Throwable $e
     * @return Response
     */
    public function render($request, Throwable $e): Response
    {
        if(method_exists($e, 'getStatusCode')){
            $this->httpStatus = $e->getStatusCode();
        }
        return show(config('status.error'),$e->getMessage(),[],$this->httpStatus);
    }
}
<?php
namespace app\common\model;

use think\facade\Cache;

class Redis
{
    /**
     * redis 切库
     * @param int $num 库号（0~15）
     */
    public static function select($num=0)
    {
        Cache::store('redis')->handler()->select($num);
    }

}
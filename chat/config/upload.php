<?php

return [
    //定义允许上传文件后缀的数组
    'suffix_arr'    =>  [
        //允许图片上传的后缀
        'image'     =>  ['jpg','jpeg','png','gif'],
        //允许上传文件的后缀
        'file'      =>  ['zip','gz','doc','txt','pdf','xls'],
        //...
    ],

    //定义允许上传文件大小的数组
    'size_arr'      =>  [
        //允许图片上传的大小M
        'image'     =>  10 * 1024 * 1024,
        //允许文件上传的大小M
        'file'      =>  50 * 1024 * 1024,
    ],
    // 发送图片返回路径前缀
    'suffix_http' => 'http://localhost/chat/public'
];
<?php
/**
 * 该文件主要存储业务状态码相关配置
 */
return [
    "success" => 1, // 成功
    "error" => 0, // 失败
    "not_login" => -1, // 没有登录，登录失败
    "user_is_register" => -2, // 用户已经被注册
    "not_found" => -3, // 找不到
];
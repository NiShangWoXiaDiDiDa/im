# IM及时通信DEMO

#### 介绍
前端使用uniapp开发 后台API使用TP6框架 后台websocket服务使用workerman的GetwayWorker长连接框架

#### 软件架构

后端使用API使用 php7.3 + TP6 + redis + JWT
websocket长连接服务使用 getwayWorker
前端使用uniapp开发，外部组件使用colorUI


#### 安装教程

1.  git后 先创建数据执行chat.sql

2.  配置TP6框架的数据库和发送图片显示url前缀
	chat->.env 文件配置数据库连接
	chat->config->upload->suffix_http 模块配置前缀

3.  配置redis(这里使用redis做登录使用，没有安装的可以修改源码login使用session替换)
	chat->config->cache->redis 模块配置Redis连接

4. 	配置多应用访问模块域名绑定模块
	chat->config->app->domain_bind 模块下的 api.chat.com(自己服务器解析的域名地址) = api(对应的应用模块)

5.	配置uniapp中请求地址
	IM->lib->config.js
	
6.  启动TP6 
	cd chat (chat目录下)
	php think run	(启动命令)

7.  启动getwayWorker长连接服务行即可，)
	chat\vendor\GatewayWorker\start_for_win.bat
	windows 双击运行即可
	linux sh start_for_win.bat

8.  运行uniapp 注册账号使用
#### 使用说明

1.  目前只支持C2C 聊天 功能简单 思路清晰 可供研究学习使用 纯个人爱好作品
附上两张应用图 。。 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0903/165923_b64df1cc_1715275.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0903/172319_c6653987_1715275.png "屏幕截图.png")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

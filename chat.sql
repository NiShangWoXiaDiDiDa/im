/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : chat

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-09-03 14:07:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for chat_communication
-- ----------------------------
DROP TABLE IF EXISTS `chat_communication`;
CREATE TABLE `chat_communication` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `fromid` int(5) NOT NULL,
  `fromname` varchar(50) NOT NULL,
  `toid` int(5) NOT NULL,
  `toname` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `time` int(10) NOT NULL,
  `shopid` int(5) DEFAULT NULL,
  `isread` tinyint(2) DEFAULT '0',
  `type` tinyint(2) DEFAULT '1' COMMENT '1是普通文本，2是图片',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of chat_communication
-- ----------------------------
INSERT INTO `chat_communication` VALUES ('62', '94', '张三', '95', '李四', '李四在不在', '1597746124', null, '1', '1');
INSERT INTO `chat_communication` VALUES ('63', '95', '李四', '94', '张三', '在的 有什么事情嘛？', '1597746139', null, '1', '1');
INSERT INTO `chat_communication` VALUES ('72', '94', '张三', '95', '李四', 'http://localhost/chat/public/uploads/20200902/8dd9304faf664d6260b729e387f2f8f7.jpg', '1599033408', null, '1', '2');
INSERT INTO `chat_communication` VALUES ('73', '94', '张三', '95', '李四', 'http://localhost/chat/public/uploads/20200902/63c410bcf46fd29bf7c10c7dca8967ab.jpg', '1599033484', null, '1', '2');
INSERT INTO `chat_communication` VALUES ('74', '94', '张三', '95', '李四', 'http://localhost/chat/public/uploads/20200902/8172fa521e1f2224d0e53ea402d6e590.jpg', '1599033526', null, '1', '2');
INSERT INTO `chat_communication` VALUES ('75', '94', '张三', '95', '李四', 'http://localhost/chat/public/uploads/20200902/24fb56a3874f8dd95cf4d1b9817f4245.jpg', '1599033585', null, '1', '2');
INSERT INTO `chat_communication` VALUES ('76', '94', '张三', '95', '李四', 'http://localhost/chat/public/uploads/20200902/78a81f43ce0feb9adf8150d15589ff2f.jpg', '1599033629', null, '1', '2');
INSERT INTO `chat_communication` VALUES ('77', '94', '张三', '95', '李四', 'nihao', '1599036496', null, '1', '1');
INSERT INTO `chat_communication` VALUES ('78', '94', '张三', '95', '李四', '还好吗', '1599036898', null, '1', '1');
INSERT INTO `chat_communication` VALUES ('79', '94', '张三', '95', '李四', '真的吗', '1599036925', null, '1', '1');
INSERT INTO `chat_communication` VALUES ('80', '94', '张三', '95', '李四', '真的吗', '1599036925', null, '1', '1');
INSERT INTO `chat_communication` VALUES ('81', '94', '张三', '95', '李四', '你好哇', '1599036936', null, '1', '1');
INSERT INTO `chat_communication` VALUES ('82', '94', '张三', '95', '李四', '你好哇', '1599036936', null, '1', '1');
INSERT INTO `chat_communication` VALUES ('83', '94', '张三', '95', '李四', '你好哇', '1599036936', null, '1', '1');
INSERT INTO `chat_communication` VALUES ('84', '94', '张三', '95', '李四', 'http://localhost/chat/public/uploads/20200902/221e92d49c0b5b37eabc3019892ab977.jpg', '1599037003', null, '1', '2');
INSERT INTO `chat_communication` VALUES ('85', '94', '张三', '95', '李四', 'http://localhost/chat/public/uploads/20200902/5147472e67e5c3568ab2487492162d8d.jpg', '1599037086', null, '1', '2');
INSERT INTO `chat_communication` VALUES ('86', '94', '张三', '95', '李四', 'http://localhost/chat/public/uploads/20200902/5b1a21a79d92b448ea507e26ebb48246.jpg', '1599037124', null, '1', '2');
INSERT INTO `chat_communication` VALUES ('87', '94', '张三', '95', '李四', 'http://localhost/chat/public/uploads/20200902/98d277a090935bdf9a2d59ca32ca5b97.jpg', '1599037163', null, '1', '2');
INSERT INTO `chat_communication` VALUES ('88', '94', '张三', '95', '李四', 'http://localhost/chat/public/uploads/20200902/8f30706d10eccf2ec9b1aa7b3b345ee4.jpg', '1599037205', null, '1', '2');
INSERT INTO `chat_communication` VALUES ('89', '94', '张三', '95', '李四', 'http://localhost/chat/public/uploads/20200902/ca6badd6115a09d19bd2eb777d29817c.jpg', '1599037219', null, '1', '2');
INSERT INTO `chat_communication` VALUES ('90', '94', '张三', '95', '李四', 'http://localhost/chat/public/uploads/20200902/3f1cc857595eef38876b44d9bd163d59.jpg', '1599037252', null, '1', '2');
INSERT INTO `chat_communication` VALUES ('91', '94', '张三', '95', '李四', 'http://localhost/chat/public/uploads/20200902/0f4d3c79ef7e52b891955d172cbfc271.jpg', '1599037341', null, '1', '2');
INSERT INTO `chat_communication` VALUES ('92', '94', '张三', '95', '李四', '李四你好哇', '1599041819', null, '1', '1');
INSERT INTO `chat_communication` VALUES ('93', '94', '张三', '95', '李四', '李四你好哇', '1599041819', null, '1', '1');
INSERT INTO `chat_communication` VALUES ('108', '95', '李四', '94', '张三', '[微笑]', '1599096189', null, '0', '1');
INSERT INTO `chat_communication` VALUES ('109', '95', '李四', '94', '张三', '你好哇[色]', '1599097008', null, '0', '1');
INSERT INTO `chat_communication` VALUES ('110', '95', '李四', '94', '张三', '啥玩意[色][色]啊啊', '1599097054', null, '0', '1');

-- ----------------------------
-- Table structure for chat_user
-- ----------------------------
DROP TABLE IF EXISTS `chat_user`;
CREATE TABLE `chat_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `mpid` int(10) DEFAULT NULL COMMENT '公众号标识',
  `openid` varchar(255) DEFAULT NULL COMMENT 'openid',
  `nickname` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '昵称',
  `headimgurl` varchar(255) DEFAULT NULL COMMENT '头像',
  `sex` tinyint(1) DEFAULT NULL COMMENT '性别',
  `subscribe` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否关注',
  `subscribe_time` int(10) DEFAULT NULL COMMENT '关注时间',
  `unsubscribe_time` int(10) DEFAULT NULL COMMENT '取消关注时间',
  `relname` varchar(50) DEFAULT NULL COMMENT '真实姓名',
  `signature` text COMMENT '个性签名',
  `mobile` varchar(15) DEFAULT NULL COMMENT '手机号',
  `is_bind` tinyint(1) DEFAULT '0' COMMENT '是否绑定',
  `language` varchar(50) DEFAULT NULL COMMENT '使用语言',
  `country` varchar(50) DEFAULT NULL COMMENT '国家',
  `province` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '省',
  `city` varchar(50) DEFAULT NULL COMMENT '城市',
  `remark` varchar(50) DEFAULT NULL COMMENT '备注',
  `group_id` int(10) DEFAULT '0' COMMENT '分组ID',
  `groupid` int(11) DEFAULT '0' COMMENT '公众号分组标识',
  `tagid_list` varchar(255) DEFAULT NULL COMMENT '标签',
  `score` int(10) DEFAULT '0' COMMENT '积分',
  `money` decimal(10,2) DEFAULT '0.00' COMMENT '金钱',
  `latitude` varchar(50) DEFAULT NULL COMMENT '纬度',
  `longitude` varchar(50) DEFAULT NULL COMMENT '经度',
  `location_precision` varchar(50) DEFAULT NULL COMMENT '精度',
  `type` int(11) DEFAULT '0' COMMENT '0:公众号粉丝1：注册会员',
  `unionid` varchar(160) DEFAULT NULL COMMENT 'unionid字段',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `last_time` int(10) DEFAULT '586969200' COMMENT '最后交互时间',
  `parentid` int(10) DEFAULT '1' COMMENT '非扫码用户默认都是1',
  `isfenxiao` int(8) DEFAULT '0' COMMENT '是否为分销，默认为0，1,2,3，分别为1,2,3级分销',
  `totle_earn` decimal(8,2) DEFAULT '0.00' COMMENT '挣钱总额',
  `balance` decimal(8,2) DEFAULT '0.00' COMMENT '分销挣的剩余未提现额',
  `fenxiao_leavel` int(8) DEFAULT '2' COMMENT '分销等级',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `update_time` datetime DEFAULT NULL COMMENT '最后一次修改时间',
  `isvalid` int(2) DEFAULT '1' COMMENT '是否有效 0无效 1有效',
  `datavision` int(11) DEFAULT '0' COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8 COMMENT='公众号粉丝表';

-- ----------------------------
-- Records of chat_user
-- ----------------------------
INSERT INTO `chat_user` VALUES ('93', '1', null, '小明', 'http://localhost/chat/public/static/newcj/img/123.jpg', '1', '0', null, null, null, null, null, '0', null, null, null, null, null, '0', '0', null, '0', '0.00', null, null, null, '0', null, 'e10adc3949ba59abbe56e057f20f883e', '586969200', '1', '0', '0.00', '0.00', '2', '2020-08-17 16:49:41', null, '1', '0');
INSERT INTO `chat_user` VALUES ('94', '1', null, '张三', 'http://localhost/chat/public/static/newcj/img/123.jpg', '1', '0', null, null, null, null, null, '0', null, null, null, null, null, '0', '0', null, '0', '0.00', null, null, null, '0', null, 'e10adc3949ba59abbe56e057f20f883e', '586969200', '1', '0', '0.00', '0.00', '2', '2020-08-17 16:51:16', null, '1', '0');
INSERT INTO `chat_user` VALUES ('95', '1', null, '李四', 'http://localhost/chat/public/static/newcj/img/123.jpg', '1', '0', null, null, null, null, null, '0', null, null, null, null, null, '0', '0', null, '0', '0.00', null, null, null, '0', null, 'e10adc3949ba59abbe56e057f20f883e', '586969200', '1', '0', '0.00', '0.00', '2', '2020-08-17 16:51:48', null, '1', '0');
INSERT INTO `chat_user` VALUES ('96', '1', null, '王五', 'http://localhost/chat/public/static/newcj/img/123.jpg', '1', '0', null, null, null, null, null, '0', null, null, null, null, null, '0', '0', null, '0', '0.00', null, null, null, '0', null, 'e10adc3949ba59abbe56e057f20f883e', '586969200', '1', '0', '0.00', '0.00', '2', '2020-08-17 16:53:59', null, '1', '0');
INSERT INTO `chat_user` VALUES ('97', '1', null, '赵六', 'http://localhost/chat/public/static/newcj/img/123.jpg', '1', '0', null, null, null, null, null, '0', null, null, null, null, null, '0', '0', null, '0', '0.00', null, null, null, '0', null, 'e10adc3949ba59abbe56e057f20f883e', '586969200', '1', '0', '0.00', '0.00', '2', '2020-08-17 16:54:54', null, '1', '0');
INSERT INTO `chat_user` VALUES ('98', '1', null, '老七', 'http://localhost/chat/public/static/newcj/img/123.jpg', '1', '0', null, null, null, null, null, '0', null, null, null, null, null, '0', '0', null, '0', '0.00', null, null, null, '0', null, 'e10adc3949ba59abbe56e057f20f883e', '586969200', '1', '0', '0.00', '0.00', '2', '2020-08-17 16:55:49', null, '1', '0');

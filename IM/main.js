import Vue from 'vue'
import App from './App'
import stroe from './store'

import msglist from './pages/msglist/msglist.vue'
Vue.component('msglist',msglist)

import myinfo from './pages/myinfo/myinfo.vue'
Vue.component('myinfo',myinfo)

import cuCustom from './components/cu-custom/cu-custom.vue'
Vue.component('cu-custom',cuCustom)

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	stroe,
    ...App
})
app.$mount()

import Vue from 'vue';
import Vuex from 'vuex';
import common from './model/common';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        //
    },
    mutations: {
        //
    },
    actions: {

    },
    modules: {
        common,
    }
});

export default store;
